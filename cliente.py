import socket


msgFromClient = "Hola, Servidor UDP"
bytesToSend = str.encode(msgFromClient)
serverAddressPort = ("127.0.0.1", 20001)
bufferSize = 1024


UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

#Envia el Mensaje
UDPClientSocket.sendto(bytesToSend, serverAddressPort)

#Respues del servidor
msgFromServer = UDPClientSocket.recvfrom(bufferSize)

msg = "Mensaje del servidor: {} ".format(msgFromServer[0])

print(msg)