import socket

localIP = "127.0.0.1"
localPort = 20001
bufferSize = 1024
msgFromServer = "Hola, cliente UDP"

bytesToSend = str.encode(msgFromServer)

UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

UDPServerSocket.bind((localIP, localPort))

print("Servidor UDP escuchando. ")

while(True):
    
    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    
    message = bytesAddressPair[0]
    address = bytesAddressPair[1]
    
    clientMsg = "Mensaje del cliente:{}".format(message)
    clientIP  = "Direccion IP del cliente:{}".format(address)
       
    print(clientMsg)
    print(clientIP)

    UDPServerSocket.sendto(bytesToSend, address)